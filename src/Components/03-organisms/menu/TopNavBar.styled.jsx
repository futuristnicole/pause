import styled from 'styled-components';
import { BrandRed, BrandBlack, ColorWhite } from '../../00-base/colors.styles';
import { Link } from 'react-router-dom';
import { BreakM } from '../../00-base/breakpoints';


export const Topbar = styled.div`
    height: 6rem;
    background-color: ${BrandBlack};
    border-bottom: 3px solid ${BrandRed}; 
    color: ${ColorWhite};
    display: flex;
    justify-content: space-between;
    align-items: center;
    @media  (mix-width: ${BreakM}) {
        height: 7rem;
        align-items: center;
}
`;
export const NavTopul = styled.ul`
    font-size: 1.4em;
    list-style: none;
    flex-direction: column;
    align-items: baseline;
    display: none;
    @media  (min-width: ${BreakM}) {
        display: flex;
        flex-direction: row;
        align-items: center;
        align-self: center;
        margin-top: 0;
        background-color: transparent;
    //  background-color: pink;
}
`;
export const Hide = styled.div`
    height: 6rem;
    width: 6rem;
    // background-color: green;
    display: flex;
    @media  (min-width: ${BreakM}) {
        display: none;
    }
`;


export const LogoBox = styled(Link)`
    margin-left: 3rem;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 3em;
`;