import React from 'react';

import WallCard from '../../02-molecules/WallCard/WallCard';


const WallsPreview = ({ id, title, count }) => (
    <div className=''>
        <WallCard key={id} title={title} count={count} />
    </div>
);

export default WallsPreview;