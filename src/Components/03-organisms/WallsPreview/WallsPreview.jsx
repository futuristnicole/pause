import React from 'react';

import WallCard from '../../02-molecules/WallCard/WallCard';

import './Wall-preview.styles.scss';
import '../../04-templates/sections/grid-4.scss';

const WallsPreview = ({ title, items }) => (
    <div className=''>
      <h1 className='title'>{title.toUpperCase()}</h1>
      <div className='grid-4'>
        {items
          .filter((item, idx) => idx < 4)
          .map(({ id, ...otherItemProps }) => (
            <WallCard key={id} {...otherItemProps} />
          ))}
      </div>
    </div>
);

export default WallsPreview;