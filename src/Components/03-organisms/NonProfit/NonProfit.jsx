import React from 'react';
import { FlexContainer, FlexBox, } from '../../04-templates/sections/flex';
import { LogoText } from '../../01-atom/Logo/LogoText';
import { NonProfitSection, Text, ImgContainer } from './NonProfit.styled';


const NonPorfit = () => {
    return (
        <NonProfitSection>
  
        <FlexContainer>
          <FlexBox>
            <Text>
            <LogoText read /> Inc. is an organized exclusively for charitable, educational and scientific puposes. We are a Florida not-for-profit with 501(c)(3) pending.     
            </Text>
          </FlexBox>
          <FlexBox>
            <ImgContainer src="../img/AboutPage/NonPorfit.png" alt="Randy and Koko" />
          </FlexBox>
        </FlexContainer>
      </NonProfitSection>
    );
};

export default NonPorfit;
