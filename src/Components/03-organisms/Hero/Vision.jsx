import React from 'react';

import { BackgroundVision, HeroCenterText, VisionTextMain, VisionTextSub } from './Hero.styled';

const HeroHome = () => {
    return (
        <div>
        <BackgroundVision>
            <HeroCenterText>
                <h1>
                    <VisionTextMain>Vision</VisionTextMain>
                    <VisionTextSub>Every person has the ability and resources to be able to read, write, and learn in any language they choice.</VisionTextSub>
                </h1>
            </HeroCenterText>
        </BackgroundVision>
       </div> 
    );
};

export default HeroHome; 