import React from 'react';

import { BackgroundHero, HeroCenterText, HeroTextMain, HeroTextSub } from './Hero.styled';
import { LogoText } from '../../01-atom/Logo/LogoText';

const HeroHome = () => {
    return (
        <div>
        <BackgroundHero>
            <HeroCenterText>
                <h1>
                    <HeroTextMain><LogoText/></HeroTextMain>
                    <HeroTextSub>The Kicking Reading Program</HeroTextSub>
                    {/* <HeroTextSub>The Kick Ass Reading Program.</HeroTextSub> */}
                </h1>
            </HeroCenterText>
        </BackgroundHero>
       </div> 
    );
};

export default HeroHome; 
