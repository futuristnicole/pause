import React from 'react';

// import IconBox from '../../02-molecules/IconBox/IconBox'
import Points from './point/Points'
import { LogoText } from './../../01-atom/Logo/LogoText'

import { Section, Title } from './WhyRB.styled';
import '../../04-templates/sections/grid-3.scss';

const WhyRB = () => {
    return (
        <Section>
            <Title>Why <LogoText read />?</Title> <br/><br/>
            <div className="grid-3-1" >
                <Points />
            </div>
       </Section> 
    );
};

export default WhyRB; 
