import styled from 'styled-components';
import { BrandRed, ColorWhite } from '../../00-base/colors.styles';


export const Section = styled.div`
    padding: 4em;
    // margin-top: -6em;
    text-align: center; 
    background-color: ${BrandRed};
`;

export const Title = styled.h2`
    display: inline-block;
    color: ${ColorWhite};
    letter-spacing: .4rem;
    transition: all .2s;
    // font-size: 2.3em;
`;