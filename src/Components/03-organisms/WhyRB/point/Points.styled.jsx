import styled from 'styled-components';

export const Card = styled.div`
    // border-radius: 3px;
    padding: 1em;
    font-size: 5em;
    margin: 0.5em;
    text-align: center;
`;