import React from 'react';
// import CustomButton from '../../01-atom/custom-button/custom-button';

import { CenterText, MissionTextMain, MissionTextSub } from './MissionHome.styled';

// import {  } from './AboutHome.styles';


const AboutHome = () => (
    <div>    
        <CenterText>
            
                <MissionTextMain>Mission</MissionTextMain>
                <MissionTextSub>To increase global literacy rates by identifying people with language disorders, providing needed information on the disorder and aid in treatment and management of the disorder. </MissionTextSub>
                

        </CenterText>
       </div> 
);

export default AboutHome;