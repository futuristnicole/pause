// text-align: center;
import styled from 'styled-components';

export const CenterText = styled.p`
    text-align: center;
    padding: 4em;
    // padding-bottom: 3em;
`;


export const MissionTextMain = styled.h2`
    display: block;
    // font-size: 2.3em;
`;

export const MissionTextSub = styled.p`
    display: block;
    // font-size: 1em;
    padding-top: 1em;
`;