const WallCardData = [
    {
      id: 1,
      title: '3 Letter Words Starting With B',
      routeName: '3_Letter_Words_Starting_With_B',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 12,
      words: [
        {
          id: 101,
          word: 'bat',
        },
        {
          id: 102,
          word: 'ban',
        },
        {
          id: 103,
          word: 'bin',
        },
        {
          id: 104,
          word: 'bit',
        },
        {
          id: 105,
          word: 'bit',
        }
      ]
    },
    {
      id: 2,
      title: '3 Letter Words Starting With D',
      routeName: '3_Letter_Words_Starting_With_D',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 10,
      words: [
        {
          id: 201,
          word: 'bat',
        },
        {
          id: 202,
          word: 'ban',
        },
        {
          id: 203,
          word: 'bin',
        },
        {
          id: 204,
          word: 'bit',
        },
        {
          id: 205,
          word: 'bit',
        }
      ]
    },
    {
      id: 3,
      title: '3 Letter Words Starting With T',
      routeName: '3_Letter_Words_Starting_With_t',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 7,
      words: [
        {
          id: 301,
          word: 'tip',
        },
        {
          id: 302,
          word: 'ten',
        },
        {
          id: 303,
          word: 'tap',
        },
        {
          id: 304,
          word: 'tan',
        },
        {
          id: 305,
          word: 'tin',
        }
      ]
    },
    {
      id: 4,
      title: '3 Letter Words Starting With N',
      routeName: '3_Letter_Words_Starting_With_n',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 9,
      words: [
        
      ]
    },
    {
      id: 5,
      title: '3 Letter Words Starting With M',
      routeName: '3_Letter_Words_Starting_With_M',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 8,
      words: [
        
      ]
    },
    {
      id: 6,
      title: 'Fry Words - 1 to 10 words',
      routeName: 'fry_words_1-10',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 10,
      words: [
        {
          id: 201,
          word: 'bat',
        },
        {
          id: 202,
          word: 'ban',
        },
        {
          id: 203,
          word: 'bin',
        },
        {
          id: 204,
          word: 'bit',
        },
        {
          id: 205,
          word: 'bit',
        }
      ]
    },
    {
      id: 7,
      title: 'Fry Words - 11 to 20',
      routeName: 'fry_words_11-20',
      imageUrl: 'https://i.ibb.co/ZYW3VTp/brown-brim.png',
      count: 10,
      words: [
        {
          id: 201,
          word: 'bat',
        },
        {
          id: 202,
          word: 'ban',
        },
        {
          id: 203,
          word: 'bin',
        },
        {
          id: 204,
          word: 'bit',
        },
        {
          id: 205,
          word: 'bit',
        }
      ]
    }
    
  ];
  
  export default WallCardData;