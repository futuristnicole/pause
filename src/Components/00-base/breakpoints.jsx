export const BreakXS = '400px';
export const BreakS = '750px';
export const BreakM = '1050px';
export const BreakL = '1200px';
export const BreakXL = '1800px';
