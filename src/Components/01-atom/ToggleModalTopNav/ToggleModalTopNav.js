import React from 'react';
import useToggle from '../../00-base/Hooks/useToggle';
import ModalTopNav from '../../02-molecules/TopNav/ModalTopNav';

import { Hambuger, HambugerH1 } from './ToggleModalNav.styled';

function ToggleModalTopNav() {
    const [isHappy, toggleIsHappy] = useToggle(true);
    return (
        <div>
            <HambugerH1 onClick={toggleIsHappy}>
                {isHappy ?   <Hambuger></Hambuger> : <> <Hambuger></Hambuger><ModalTopNav />< /> }
            </HambugerH1>
        </div>
    );
}

    
//     const [isTopNav, toggleIsTopNav] = useToggle(false);
    
//     return (
       
//     <div>
//         <HambugerH1 onClick={toggleIsTopNav}>{isTopNav ?  <><div className="hambuger"></div><ModalTopNav /> < /> : <div className="hambuger"></div>}</HambugerH1>

//     </div>
//     )
// }

export default ToggleModalTopNav;