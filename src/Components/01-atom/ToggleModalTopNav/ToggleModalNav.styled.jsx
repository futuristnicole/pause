import styled from 'styled-components';
import { BreakM } from '../../00-base/breakpoints';

export const HambugerH1 = styled.h1`
    display: flex;
    height: 6rem;
    width: 6rem;
    // background-color: goldenrod;
    
    @media  (min-width: ${BreakM}) {
        display: none;
    }
`   

export const Hambuger = styled.div`
    height: 6rem;
    width: 6rem;
    // background-color: goldenrod;
    background-image: url(../../img/icon/MenuTop.jpg);
    background-position: center;
    background-size: cover;
    margin-right: 2rem;
    @media  (min-width: ${BreakM}) {
        display: none;
        margin-right: 0;
}
`   