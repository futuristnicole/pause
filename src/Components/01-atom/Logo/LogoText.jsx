import React, { Fragment } from 'react';

import { LogoRead, LogoBoot } from './LogoText.styles';

export const LogoText = ({ ...props }) => (
  <Fragment>
    <LogoRead {...props} >READ</LogoRead><LogoBoot {...props} >BOOT</LogoBoot>
  </Fragment>
);

