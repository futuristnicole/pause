import styled, { css } from 'styled-components';
// import colors from '../../00-base/colors.styles';
import { BrandRed, BrandBlack, ColorWhite } from '../../00-base/colors.styles';
import { FontFamilyLogo } from '../../00-base/font.styles';


export const LogoText = css`
    font-family: ${FontFamilyLogo};
`;
export const LogoReadRed = css`
    ${LogoText}
    color: ${BrandRed};
`;

export const LogoReadWhite = css`
    ${LogoText}
    color: ${ColorWhite};
`;

const getReadColor = props => {
  
    return props.read ? LogoReadWhite : LogoReadRed;
  };

export const LogoRead = styled.span`
    ${LogoText}
    ${getReadColor}
`;




export const LogoBootBLack = css`
    ${LogoText}
    color: ${BrandBlack};
`;
export const LogoBootWhite = css`
    ${LogoText}
    color: ${ColorWhite};
`;

const getBootColor = props => {
  
    return props.boot ? LogoBootWhite : LogoBootBLack;
  };

export const LogoBoot = styled.span`
    ${LogoText}
    ${getBootColor}
`;