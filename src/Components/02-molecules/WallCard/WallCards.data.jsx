import React, { Component } from 'react';
import { graphql } from 'react-apollo';


import WallCard from './WallCard';
import WallCards from '../../../Queries/WallCards.querie';

class WallCardData extends Component {
    renderWordWalls() {
        return this.props.data.WallCard.map( wallCard => {
            return(
                <div key={wallCard.id} >  
                {/* {wallCard.location} */}
                    <WallCard 
                        id={wallCard.id}
                        title={wallCard.title}
                        // alt={wallCard.alt} 
                        // location={wallCard.location} 
                        // size={wallCard.size} 
                    />
                </div>
            )
        });
    }
    
    render() {
        console.log(this.props);
        if (this.props.data.loading) { return <div>Loading...</div>; } 
        return (
            
            <section >
                {this.renderWordWalls()}

            </section>
        );
    }
}

export default graphql(WallCards)(WallCardData);