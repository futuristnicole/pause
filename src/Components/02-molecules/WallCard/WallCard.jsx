import React  from 'react';

// import WordCount from '../../01-atom/WordCounter/WordCounter';
// import { CardGrid, BackgroundImage, Title, Text } from './WallCard.styled';

import './collection-item.styles.scss';
import { CardGrid, CardIMG, Title, Count } from './WallCard.styled';

const WallCard  = ({ id, title, count, imageUrl }) => (
  <CardGrid>
    <CardIMG />
    <div>
        <Title>{title}</Title>
    </div>
    <Count>{count}</Count> 
  </CardGrid>

  );
  
export default WallCard;