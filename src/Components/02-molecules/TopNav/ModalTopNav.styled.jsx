import styled from 'styled-components';
import { BreakM } from '../../00-base/breakpoints';

export const NavTopulModal = styled.ul`
    font-size: 1.4em;
    list-style: none;
    flex-direction: column;
    align-items: baseline;
    display: flex;
    @media  (min-width: ${BreakM}) {
        display: None;
        flex-direction: row;
        align-items: center;
        align-self: center;
        margin-top: 0;
        background-color: transparent;
    //  background-color: pink;
}
`;
export const ModalNav = styled.div`
   background-color: #222;
    position: absolute;
    top: 6rem;
    right: 0; 
    @media (min-width: ${BreakM}) {
      
        display: none; } 
`;
