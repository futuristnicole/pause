import React from 'react'
import ReactDOM from 'react-dom';
// import NavItem from '../Nav-item';
import TopNav from './TopNav';
import { ModalNav } from './ModalTopNav.styled';
import { GlobalStyle } from '../../../App/global.styles';
import { GlobalFontStyle } from '../../../App/globalFont.styles';

const ModalTopNav = props => {
    return ReactDOM.createPortal(
        <ModalNav>
        <GlobalStyle /> <GlobalFontStyle />
            <ul className="nav-top_modal">
                <TopNav />
            </ul>
        </ModalNav>,
        document.querySelector('#modalTopNav')
    )
};

export default ModalTopNav;