import styled from 'styled-components';
import { ColorWhite } from '../../00-base/colors.styles';


export const ImgContain = styled.img`
    background-size: cover;
    width: 100%; 
    width: 60%;
    margin: 0 auto;
    width: 100%;
    object-fit: cover;
    display: block;
    height: 100px;
`;

export const Point = styled.p`
    color: ${ColorWhite};
    font-size: 2em;
    padding: 1em;
`;