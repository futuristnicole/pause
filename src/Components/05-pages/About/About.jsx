import React from 'react';
import Vision from '../../03-organisms/Hero/Vision';
import Mission from '../../03-organisms/Mission/MissionHome';
import NonPorfit from '../../03-organisms/NonProfit/NonProfit';

const AboutPage = () => (
    <div className=''>
        
        <Vision />
        <Mission />
        <NonPorfit />
        <h2>Research</h2>
        <h2>People</h2>
        <h3>Officers</h3>
        <h3>Board of Directors</h3>
    </div>
);

export default AboutPage;