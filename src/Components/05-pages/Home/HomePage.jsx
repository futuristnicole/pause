import React from 'react';
import HeroHome from '../../03-organisms/Hero/HeroHome';
import AboutHome from '../../03-organisms/Mission/MissionHome';
import WhyRB from '../../03-organisms/WhyRB/WhyBR';

const HomePage = () => (
    <div className=''>
        <HeroHome />
        <AboutHome />
        <WhyRB />
    </div>
);

export default HomePage;