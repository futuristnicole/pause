import styled from 'styled-components';
import { ColorWhite } from '../../00-base/colors.styles';

export const Section = styled.div`
    padding: 0;
    margin-top: 0;
    text-align: center; 
    background-color: ${ColorWhite};
    @media screen and (min-width: ${BreakS}) {
        padding: 4em;
    }
`;