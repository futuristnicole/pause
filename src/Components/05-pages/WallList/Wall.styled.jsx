import styled from 'styled-components';
import { ColorWhite } from '../../00-base/colors.styles';
import { BreakS } from '../../00-base/breakpoints';

export const Title = styled.h1`
    padding: .5em;
    // margin-top: -6em;
    text-align: center; 
    // background-color: ${ColorWhite};
`;

export const Section = styled.div`
    padding: 0;
    margin: 0;
    text-align: center; 
    background-color: ${ColorWhite};
    @media screen and (min-width: ${BreakS}) {
        padding: 4em;
    }
`;