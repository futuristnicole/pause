import React from 'react';
// import WallCardHolder from '../../03-organisms/WallCardHolder/WallCardHolder';
// import { Section } from './WallList.styled';
import WALL_DATA from '../../Data/WallData';
import CollectionPreview from '../../03-organisms/WallsPreview/WallsPreview';

import {Section} from './WallList.styled'


class WallList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collections: WALL_DATA
          };
        }
        render() {
            const { collections } = this.state;
            return (
              <Section >
                {collections.map(({ id, ...otherCollectionProps }) => (
                  <CollectionPreview key={id} {...otherCollectionProps} />
                ))}
              </Section>
            );
          }
        }
    // <Section>
    //     <h1>Wall List</h1>
    //     <WallCardHolder />
    // </Section>
    

export default WallList;