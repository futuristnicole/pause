import React from 'react';
// import WallCardHolder from '../../03-organisms/WallCardHolder/WallCardHolder';
// import { Section } from './WallList.styled';
import WALL_DATA from '../../Data/WallData';
import CollectionPreview from '../../03-organisms/WallsPreview/WallCollection';

import { Section, Title } from './Wall.styled'
import { WallsGrid } from '../../04-templates/sections/WallsGrid';



class WallList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collections: WALL_DATA
          };
        }
        render() {
            const { collections } = this.state;
            return (
              <Section >
                <Title>Word Walls</Title>
                <WallsGrid>
                    {collections.map(({ id, ...otherCollectionProps }) => (
                    <CollectionPreview key={id} {...otherCollectionProps} />
                    ))}
                </WallsGrid>
              </Section>
            );
          }
        }

export default WallList;