import React from 'react';

import SignIn from '../../02-molecules/sign-in/Sign-in';
import SignUp from '../../02-molecules/sign-up/sign-up';

import { SignInAndSignUpContainer } from './sign-in-and-sign-up.styles';

const SignInAndSignUpPage = () => (
  <SignInAndSignUpContainer>
    <SignIn />
    <SignUp />
  </SignInAndSignUpContainer>
);

export default SignInAndSignUpPage;