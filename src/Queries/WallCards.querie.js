import gql from "graphql-tag";

const WordWallQuerie = gql`
  query WordWall {
    WordWall  {
        id
        title
        img
        order
        }
    }
`;

export default WordWallQuerie;