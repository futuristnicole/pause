import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { auth, createUserProfileDocument } from '../firebase/firebase.utils'
import { setCurrentUser } from '../redux/user/user.actions';
// import { selectCurrentUser } from '../redux/user/user.selectors';


import Header from '../Components/03-organisms/menu/TopNavBar';
import SignInAndSignUpPage from '../Components/05-pages/sign-in-and-sign-up/sign-in-and-sign-up';
import Home from '../Components/05-pages/Home/HomePage';
import About from '../Components/05-pages/About/About';
import WallList from '../Components/05-pages/WallList/AllWall';
import Footer1 from '../Components/03-organisms/Footer/Footer';

import { GlobalStyle } from './global.styles';
import { GlobalFontStyle } from './globalFont.styles';



class App extends React.Component  {
  unsubscribeFromAuth = null;

  componentDidMount() {
    const { setCurrentUser } = this.props;

    this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      if (userAuth) {
        const userRef = await createUserProfileDocument(userAuth);

        userRef.onSnapshot(snapShot => {
          setCurrentUser({
            id: snapShot.id,
            ...snapShot.data()
          });
        });
      }

      setCurrentUser(userAuth);
    });
  }

  componentWillUnmount() {
    this.unsubscribeFromAuth();
  }

  render() {
    return (
      <div>
        <GlobalStyle /> <GlobalFontStyle />
        <Header />
        <Switch>
          <Route exact path="/"  component={Home} />
          <Route exact path="/about"  component={About} />
        {/* <Route  path="/topic/:topicid"  component={TopicDetail} /> */}
          <Route path="/wordwalls"  component={WallList} />
          <Route
            exact
            path='/signin'
            render={() =>
              this.props.currentUser ? (
                <Redirect to='/' />
              ) : (
                <SignInAndSignUpPage />
              )
            }
          />
        </Switch>
        <Footer1 />
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  currentUser: user.CurrentUser
});

const mapDispatchToProps = dispatch => ({
  setCurrentUser: user => dispatch(setCurrentUser(user))
});

export default connect(
  mapStateToProps,
  // null,
  mapDispatchToProps
)(App);