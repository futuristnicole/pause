import { createGlobalStyle } from 'styled-components';
import { BrandBlack } from '../Components/00-base/colors.styles';
import { FontFamilyMain, FontSizeBase } from '../Components/00-base/font.styles';

export const GlobalStyle = createGlobalStyle`
    *,
    *::after,
    *::before {
        margin: 0;
        padding: 0;
        box-sizing: inherit;
    }   
    body {
	box-sizing: border-box;	
	}

    html {
        width: 100%;
        height: 100%;
        perspective: 900;
        font-family: ${FontFamilyMain};
        color: ${BrandBlack};
        font-size: ${FontSizeBase};
    }
    
	a {
		text-decoration: none;
		// color: black;
	}
  
    .wrap {
        padding: 10px;
        max-width: 1140px;
        width: 100%;
        margin: 0 auto;
      }

      ul {
		text-decoration: none;
      }
      li {
		text-decoration: none;
      }
`;
