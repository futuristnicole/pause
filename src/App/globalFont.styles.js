import { createGlobalStyle } from 'styled-components';
// import { BrandBlack } from '../Components/00-base/colors.styles';
import { BreakS, BreakM } from '../Components/00-base/breakpoints';
// , BreakL, BreakXL
export const GlobalFontStyle = createGlobalStyle`
      h1 {
        font-size: 2.6em;
            @media screen and (min-width: ${BreakS}) {
                font-size: 3.8em;
            }
            @media screen and (min-width: ${BreakM}) {
                font-size: 4.4em;
            }
            
        }
        h2 {
            font-size: 2.2em;
            // font-weight: normal;
            @media screen and (min-width: ${BreakS}) {
                font-size: 3.2em;
            }
            @media screen and (min-width: ${BreakM}) {
                font-size: 3.6em;
            }
        }
        h3 {
            font-size: 1.8em;
            color: lighten($color-black, 5%);
            @media screen and (min-width: ${BreakS}) {
                font-size: 2em;
            }
            @media screen and (min-width: ${BreakM}) {
                font-size: 2.2em;
            }
        }
        
        h4 {
            font-weight: normal;
            font-size: 1.6em;
            @media screen and (min-width: ${BreakS}) {
                font-size: 1.8em;
            }
            @media screen and (min-width: ${BreakM}) {
                font-size: 2em;
            }
        }
        
        h5 {
            font-size: 1.4em;
            font-weight: normal;
            @media screen and (min-width: ${BreakM}) {
                font-size: 1.8em;
            }   
        }
        h6 {
            font-size: 1.2em;
            font-weight: normal;    
            @media screen and (min-width: ${BreakM}) {
                font-size: 1.6em;
            }
            
        
        }
        
        p,
        li {
            font-weight: normal;    
            font-size: 1.5em;
            @media screen and (min-width: 1000) {
                font-size: 1.9em;
            }
        }
        ol {
            padding-left: 5em;
        }
`;
